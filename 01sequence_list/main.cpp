#include <iostream>
using std::cout;
using std::endl;

typedef struct{
    int data[20];
    int length;
}SqList;

void createSqList(SqList *L){
    //loop over and assign values
    L->length=0;
    for (int i = 0; i < 10; i++) {
        L->data[i]=i;
        L->length++;
    }
}
void printSqList(SqList L){
    for (int i = 0; i < L.length; i++) {
        cout<<L.data[i]<<", ";
    }
}

int getElement(SqList L, int i){
    if (L.length < 1 || i < 0 || i > L.length) {
        cout<<"get element error";
        return -1;
    }
    return L.data[i];
}
void listInsert(SqList *L,int i,int num){
    if (i < 0 || i > L->length || L->length == 20) {
        cout<<"insert error";
        return;
    }
    for (int j = L->length; j > i; j--) {
        L->data[j]=L->data[j-1];
    }
    L->data[i]=num;
    L->length++;
}

//初始条件，顺序线性表已存在，1<=i<=ListLength(L)
//操作结果，删除L的第i个元素，并用e返回其值，L的长度减1
void ListDelete(SqList *L, int i, int *e){
    int k;
    if(L->length==0)
        return;
    if (i < 0 || i > L->length) {
        return;
    }
    *e=L->data[i];
    for (int j = i; j < L->length - 1; j++) {
        L->data[j]=L->data[j+1];
    }
    L->length--;
}

int main() {
    SqList L;
    createSqList(&L);
//    //std::cout<<L.length;
//    printSqList(L);
//    //get an element;
//    cout<<endl;
//    cout<<getElement(L,5);
//
//    cout<<endl;
//    //insert
//    listInsert(&L,3,999);
//    printSqList(L);

    int e=0;
    ListDelete(&L, 3, &e);
    cout<<e;
    cout<<endl;
    printSqList(L);
}
