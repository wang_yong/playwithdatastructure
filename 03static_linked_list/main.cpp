#include <iostream>

#define MAX 9
using std::cout;
using std::endl;
//静态链表存储结构
typedef struct {
    int data;
    int cur;
} Component, StaticLinkList[MAX];

int createStaticList(StaticLinkList space) {
    for (int i = 0; i < MAX - 1; i++) {
        space[i].cur = i + 1;
        space[i].data = -1;
    }
    space[MAX - 1].cur = 0;
    space[MAX - 1].data = -1;
    return 0;
}

int getNextIdle(StaticLinkList space) {
    int addr = space[0].cur;
    cout << addr << ",";
    if (addr) {//如果地址存在
        space[0].cur = space[addr].cur;//把第一个结点的空闲指针设置为下一个空闲结点；
    }
    cout << space[0].cur << "\n";
    return addr;
}

//在第i个结点处插入元素
int insertElement(StaticLinkList space, int i, int number) {
    //找到空闲的结点，插入
    int idleAddr = getNextIdle(space);
    space[idleAddr].data = number;
    int head = MAX - 1;

    if (i < 1) {
        return 1;
    }
    int count = 1;//记数,找到i-1的位置
    while (count < i) {//head从第1个开始算，当count=i时，说明head在i-1的位置。
        head = space[head].cur;
        count++;
    }
    space[idleAddr].cur = space[head].cur;
    space[head].cur = idleAddr;
    return number;
}

void printStaticList(StaticLinkList s) {
    for (int i = 0; i < MAX; i++) {
        cout << "(" << s[i].data << ", " << s[i].cur << "), ";
    }
}

void printActualLinkList(StaticLinkList s) {
    int head = s[MAX - 1].cur;
    while (head!= 0) {
        cout<< s[head].data<<", ";
        head=s[head].cur;
    }


}

int textBookListInsert(StaticLinkList L, int i, int e) {
    int j, k, l;
    k = MAX - 1;
    j = getNextIdle(L);
    if (j) {
        L[j].data = e;
        for (l = 1; l < i; l++) {
            k = L[k].cur;
        }
        L[j].cur = L[k].cur;
        L[k].cur = j;
        return 0;
    }
    return 1;
}

int freeSpace(StaticLinkList space, int loc) {
    //if you want to free a space, the free space now become the first idle space;
    //the first idle space's next element is the origin first one.
    space[loc].data = -111;
    space[loc].cur = space[0].cur;
    space[0].cur = loc;

    return 0;
}

int deleteFromList(StaticLinkList space, int i) {
    //check if i is a valid position
    if (i < 1) {
        return 1;
        //i is less than 1 or i is more than the length of actual list
    }
    //from the first position that does have element
    int head = MAX - 1;

    //开始从1计数
    int count = 1;
    //find the position i-1 out
    while (count < i) {
        head = space[head].cur;
        count++;
    }//from 1 to i-1 which means we find out the position i-1;
    //for instance, when count is equal to 1, We want to find the position before 1 which means it is MAX-1;

    //now we get the position i-1 is equal to head. Let's do something.
    int next = space[head].cur;
    space[head].cur = space[next].cur;
    freeSpace(space, next);

    return space[i].data;
}

int main() {
    StaticLinkList s;
    createStaticList(s);

    insertElement(s, 1, 999);
    insertElement(s, 1, 998);
    insertElement(s, 1, 997);

    deleteFromList(s, 3);
    printStaticList(s);
    printActualLinkList(s);
}
