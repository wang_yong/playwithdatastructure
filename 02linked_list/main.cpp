#include <iostream>

using std::cout;
using std::endl;
//线性表的单链表结构
typedef struct Node {
    int data;
    struct Node *next;
} Node;

typedef struct Node *LinkList;

//先来一个头插法,有头结点，传入头指针
void HeadInsertCreate(LinkList *L, int n) {
    //L是一个头指针，指向的内容是头结点的地址
    *L = (LinkList) malloc(sizeof(Node));//这里才把头结点创建出来
    (*L)->next = nullptr;
    LinkList p;
    for (int i = 0; i < n; i++) {
        p = (LinkList) malloc(sizeof(Node));
        p->data = i * 10 + i;
        p->next = (*L)->next;
        (*L)->next = p;
    }
}
//再来一个尾插法，推荐此法
void TailInsertCreate(LinkList *L,int n){
    //L 是一个头指针，指向的是头结点的地址
    *L=(LinkList)malloc(sizeof(Node));
    LinkList p,r=*L;//p是新建的结点指针，r是尾结点指针
    for (int i = 0; i < n; i++) {
        p=(LinkList)malloc(sizeof(Node));
        p->data=i*10+i;
        r->next=p;
        r=r->next;
    }
    r->next= nullptr;
}

//返回第n个位置的数据
int getElement(LinkList *head, int n){
    LinkList p=(*head)->next;
    int counter=1;
    while(p!= nullptr&&counter<n) {
        p=p->next;
        counter++;
    }
    return p->data;
}
//在第n个位置插入数据
int insertElement(LinkList *head,int n,int num){
    LinkList p=*head;
    int count=1;
    //先找到i-1这个位置
    while(p->next!= nullptr&&count<n) {
        p=p->next;
        count++;
    }
    if(p->next== nullptr||count>n){
        return -1;
    }
    auto s=(LinkList)malloc(sizeof(Node));
    s->data=num;
    s->next=p->next;
    p->next=s;
    return s->data;
}

//删除第n个位置
int deleteElement(LinkList *head, int n){
    LinkList p=*head;
    int count=1;
    while(p->next!= nullptr&&count<n) {
        p=p->next;
        count++;
    }
    if(p->next== nullptr||count>n){
        return -1;
    }
    LinkList r=p->next;
    p->next=r->next;
    return r->data;
}
//删除整个表,返回原表的长度
int deleteWhole(LinkList *head){
    LinkList p=(*head)->next;//到第一个结点
    LinkList q;
    int count=0;
    while (p!= nullptr) {
        q=p->next;
        free(p);
        p = q;
        count++;
    }
    return count;

}

void printLinkedList(LinkList *head) {
    LinkList p = (*head)->next;
    while (p != nullptr) {
        cout << p->data << ", ";
        p = p->next;
    }
}


int main() {
    LinkList headNode1;//定义头结点，在create 函数中创建实现的，这里声明
    LinkList *linklist1= &headNode1;//头指针，指向头结点的地址
    TailInsertCreate(linklist1,9);
    printLinkedList(linklist1);
    cout <<endl;

    cout<<  "insert an number:"<<endl;
    cout<<insertElement(linklist1,4,808)<<endl;
    printLinkedList(linklist1);
    cout <<endl;

    cout<<"delete an number:\n";
    deleteElement(linklist1,9);
    printLinkedList(linklist1);
    cout<< "\nthe linklist's :"<<deleteWhole(linklist1)<< " nodes has been deleted";
}
