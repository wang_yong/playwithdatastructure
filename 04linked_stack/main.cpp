#include <iostream>
using std::cout;
using std::endl;
typedef struct StackNode {
    int data;
    StackNode *next;
} StackNode, *StackPtr;

typedef struct LinkedStack {
    StackPtr top;
    int length;
} LinkedStack;

//
void initAStack(LinkedStack *S) {
    S->length = 0;
    S->top = nullptr;
}

//push data
int push(LinkedStack *s, int data) {
    auto p = (StackPtr) malloc(sizeof(StackNode));
    p->data = data;
    //接上
    p->next = s->top;
    s->top = p;
    //长度自增
    s->length++;
    return data;
}


//pop data
int pop(LinkedStack *s) {
    if (s->length < 1) {
        return -1;
    }
    int res = s->top->data;

    StackPtr q = s->top;//栈顶
    s->top = q->next;//栈顶指针，指向指针的下一个
    free(q);//释放结点
    s->length--;
    return res;
}

void printStack(LinkedStack *s) {

}


int main() {
    auto *S = (LinkedStack *) malloc(sizeof(StackNode));
    initAStack(S);
    for (int i = 0; i < 18; i++) {
        cout<<push(S,i)<<", ";
    }
    cout<<endl;
    cout<<S->length;
    cout<<endl;
    int fixedLength=S->length;
    for (int i = 0; i < fixedLength; i++) {//这里如果用S->length的话，每次弹出length都会变少，所以得到的结果会变少
        cout<<pop(S)<<", ";
    }


}
